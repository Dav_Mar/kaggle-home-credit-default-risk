#18/08/2018

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix,classification_report
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.svm import LinearSVC
from sklearn_pandas import DataFrameMapper, CategoricalImputer
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
import lightgbm as lgb




def RandomForest(X_train, y_train, X_test, y_test, n_estimators=50,criterion='gini',min_samples_split=27,min_samples_leaf=17,max_depth=10,n_jobs=-1,max_leaf_nodes=None,warm_start=False,oob_score = True,bootstrap=True):
    """esegue una classificazione randomforest ML e effettua la previsione su X_test e da come output l accuracy e la predizione"""
          
    classifier=RandomForestClassifier         (n_estimators=n_estimators,criterion=criterion,min_samples_split=min_samples_split,min_samples_leaf=min_samples_leaf,max_depth=max_depth,n_jobs=n_jobs,max_leaf_nodes=max_leaf_nodes,warm_start=warm_start,oob_score = oob_score,bootstrap=bootstrap)
    classifier.fit(X_train, y_train) 
    y_pred = classifier.predict(X_test)  
    acc=accuracy_score(y_test, y_pred, normalize=True, sample_weight=None)
    print(confusion_matrix(y_test, y_pred))  
    print(classification_report(y_test, y_pred))
    return y_pred,acc



def TFID(text_train,max_df=0.99, max_features=8000,min_df=0.000075,use_idf=False, ngram_range=(1,2),stop_words = 'english'):

          tfidf_vectorizer= TfidfVectorizer(max_df=max_df, max_features=max_features,
                                              min_df=min_df,use_idf=use_idf, ngram_range=ngram_range,stop_words = stop_words)
          hash_vectorizer = HashingVectorizer()

          tfidf_matrix = tfidf_vectorizer.fit_transform(text_train)
          hash_matrix = hash_vectorizer.fit_transform(text_train)
          feature_names = tfidf_vectorizer.get_feature_names()
          return tfidf_matrix
        
        



def SVML(X_train, X_test, y_train, y_test, multi_class='crammer_singer'):
    """esegue una classificazione supportvectormachine lineare e effettua la previsione 
    su X_test e da come output l accuracy e la predizione"""

    clf_svm = LinearSVC(multi_class = multi_class)
    clf_svm.fit(X_train,y_train)
    pred_svm = clf_svm.predict(X_test)
    print(confusion_matrix(y_test, pred_svm ))  
    print(classification_report(y_test, pred_svm ))
    acc=accuracy_score(y_test, pred_svm, normalize=True, sample_weight=None)

    return pred_svm,acc


def NaiveBayes_MultiNom(X_train, X_test, y_train, y_test, alpha=.01):
    """esegue una classificazione NaiveBayes multinom e effettua la previsione 
    su X_test e da come output l accuracy e la predizione"""
    clf = MultinomialNB(alpha=alpha)
    clf.fit(X_train,y_train) # train the classifier
    pred = clf.predict(X_test) # test the classifier
    print(confusion_matrix(y_test, pred ))  
    print(classification_report(y_test, pred ))
    acc=accuracy_score(y_test, pred, normalize=True, sample_weight=None)

    return pred,acc


def DecisionTree(X_train, X_test, y_train, y_test,criterion = "gini", random_state = 100, max_depth=3, min_samples_leaf=5):
    clf_gini = DecisionTreeClassifier(criterion = criterion, random_state = random_state,max_depth=max_depth, min_samples_leaf=min_samples_leaf)
    """esegue una classificazione decisiontree e effettua la previsione 
    su X_test e da come output l accuracy e la predizione"""
    clf_gini.fit(X_train, y_train)
    pred = clf_gini.predict(X_test) # test the classifier
    print(confusion_matrix(y_test, pred ))  
    print(classification_report(y_test, pred ))
    acc=accuracy_score(y_test, pred, normalize=True, sample_weight=None)

    return pred,acc




def textdata(f,out):
    """prende dataset X e varabile target out e crea un nuovo dataset dato da un'operazione
    sulle variabili. Ad esempio una Tfid su le variabili di testo e nulla sulle variabili
    numeriche. Ad esempio se si vuole fare un modello machine learning su un dataset
    contenente variabili di testo e altre numeriche"""
    mapper = DataFrameMapper([
         ('text_desc', TfidfVectorizer()),
         ('favourites_count', None),
         ('followers_count', None),
         ('friends_count', None),
      ('listed_count', None),
      ('statuses_count', None),
      ('age13_18', None),
      ('age19_22', None),
        ('age23_29', None),
        ('age30_65', None)
     ])


    features = mapper.fit_transform(f)
    categories = f[out]
    return features, categories





def lightgbm_prob(X, Y,X_star, learning_rate= 0.003, boosting_type= 'gbdt',
objective = 'binary',metric= 'binary_logloss',sub_feature= 0.5, num_leaves = 10, min_data= 50,
max_depth = 10, num_trees=10000):
    
   """determina le probabilità su X,Y e le calcola su X_star con modello lightgbm"""
    
   d_train = lgb.Dataset(X, label=Y)
   params = {}
   params['learning_rate'] = learning_rate
   params['boosting_type'] = boosting_type
   params['objective'] = objective
   params['metric'] = metric
   params['sub_feature'] = sub_feature
   params['num_leaves'] = num_leaves
   params['min_data'] = min_data
   params['max_depth'] = max_depth
   params['num_trees']=num_trees

   clf = lgb.train(params, d_train)

#Prediction
   y_pred=clf.predict(X_star)
   return y_pred
                  
                  
                  
                  
                  
                  
                  
                  
def RandomForest_prob(X, Y,X_star, n_estimators=50,criterion='gini',min_samples_split=27,min_samples_leaf=17,max_depth=10,n_jobs=-1,max_leaf_nodes=None,warm_start=False,oob_score = True,bootstrap=True):
    
    
    """determina le probabilità su X,Y e le calcola su X_star con modello RandomForest"""

    clf = RandomForestClassifier(n_estimators=2500,max_depth=50, random_state=0)

    classifier=RandomForestClassifier         (n_estimators=n_estimators,criterion=criterion,min_samples_split=min_samples_split,min_samples_leaf=min_samples_leaf,max_depth=max_depth,n_jobs=n_jobs,max_leaf_nodes=max_leaf_nodes,warm_start=warm_start,oob_score = oob_score,bootstrap=bootstrap)
    classifier.fit(X, Y) 


    predictions = classifier.predict_proba(X_star)
    return predictions   
                  
                  
                  
                  

def SVML_prob(X, Y,X_star):
    """determina le probabilità su X,Y e le calcola su X_star con modello SVML"""
         

    svc = LinearSVC() # fit faster than `probability=True`
    svc.fit(X,Y)
    y_margins = svc.decision_function(X_star)
    y_prob = (y_margins - y_margins.min()) / (y_margins.max() - y_margins.min())              
                  

    return y_prob                 
                  
                  
        
     