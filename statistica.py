#18/08/2018


import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd



def freq_tab(x,var):
    """fornisce la tavola di frequenza del dataset x per la variabile var"""
    return  x[var].value_counts()/x.count()[var]

def freq_tab2(x):
    """fornisce la tavola di frequenza della matrice x per la variabile var"""
    return  x.value_counts()/x.count()
                                    
                                                          
    
def var_alfanumeriche(x):
    """fornisce la tavola di frequenza per solo le variabili non numeriche del dataset x"""
    a=x.dtypes
    a=a[a.values=='object'].index

    for i in a:
        print( freq_tab(x,i))
        print("")
        print("")
    
def tipo(x):
    """dice il tipo di un oggetto"""
    return type(x)


def var(x):
    """elenca il nome di tutte le variabili del dataset x"""
    return x.dtypes.index


def presenza(x,y):
    """dirà quali variabili del dataset x sono presenti nel dataset y"""
    for i in var(x):
        print(i+":                       "+str(i in var(y)))
              
              
              
def modalità_presenza(x,y):
        """mostra a schermo le tav di frequenza per ciascuna variabile di x e di y,
        in modo da poter vedere se tutte le modalità della variabile x sono anchein y."""
        for i in var(x):
            print(freq_tab(x,i))
            print("")
            print(freq_tab(y,i))
            print("")
            print("")
            print("")
            print(i)
    

def dist_freq(x):
    """FA IL PLOT DELLA DISTRIBUZIONE DI FREQUENZA DI X"""
    sns.distplot(x);
    
    
    
def distribuzioni(df):
    """fornisce le distribuzioni per solo le variabili non numeriche del dataset x, utile per vedere se
    ci sono outliers"""

    a=df.dtypes
    a=a[a.values!='object'].index
    for i in a:
        print(str(i))
        h=dist_freq(df[i].dropna())
        plt.show()
        
        
        
def outliers1(df,sigma=3,sost=np.nan,TARGET='TARGET'):    
    """corregge gli outliers con la regola della normale e della dev.standard"""
    a=df.dtypes
    a=a[a.values!='object'].index
    a=a[a!=TARGET]

    for i in a:
        print("CORREZIONE OUTLIERS: VAR "+i)
        k=df[i].dropna()
        mean=k.mean()
        std=k.std()
        df[i] = np.where(df[i] > mean+sigma*std, sost, df[i]) 
        df[i] = np.where(df[i] < mean-sigma*std, sost, df[i]) 
    return df
    
    
def outliers2(df,k=1.5,sost=np.nan,TARGET='TARGET'):    
    """corregge gli outliers considerando i quantili"""

    a=df.dtypes
    a=a[a.values!='object'].index
    a=a[a!=TARGET]
    for i in a:
        print("CORREZIONE OUTLIERS: VAR "+i)
        g=df[i].dropna()
        q1=np.quantile(g, 0.25)
        q3=np.quantile(g, 0.75)
        f1=q3+k*(q3-q1)
        f2=q1-k*(q3-q1)
        df[i] = np.where(df[i] > f1, sost, df[i]) 
        df[i] = np.where(df[i] < f2, sost, df[i]) 
        
        
    return df     



def fattorizzazione(x):
    """fattorizza la variabile x, ad esempio x assume valore yes e no dopo assumerà 1 e 0"""
    x, _  = pd.factorize(x)
    return x



def one_hot_encoder_variabile(data, nan_as_category = True):
    """trasforma una variabile x del dataset data del tipo x=[lavoratore,disoccupato,pensionato] in tre nuovi
    variabili x1,x2,x3 dove x1=[0,1],x2=[0,1] e x3=[0,1] dove rispettivamente x1,x2 e x3 assumerà 1 se
    il record assume "lavoratore", "disoccupato" o "pensionato" e 0 altrimenti."""
    original_columns = list(data.columns)
    categorical_columns = [col for col in data.columns \
                           if not pd.api.types.is_numeric_dtype(data[col].dtype)]
    for c in categorical_columns:
        if nan_as_category:
            data[c].fillna('NaN', inplace = True)
        values = list(data[c].unique())
        for v in values:
            data[str(c) + '_' + str(v)] = (data[c] == v).astype(np.uint8)
    data.drop(categorical_columns, axis = 1, inplace = True)
    return data, [c for c in data.columns if c not in original_columns]


def one_hot_enconder(df,nan_as_category=True):
    """attiva la funzione one_hot_encoder_variabile"""
    df, _ = one_hot_encoder_variabile(df, nan_as_category=nan_as_category)
    return df

def cisonoinfiniti(Y):
    """dice se ci sono infiniti nel dataset. Se da un array vuoto vuol dire che non ci sono"""
    k=np.isfinite(Y.all())
    print(k[k.values==False])
    
def cisononan(Y):
    """dice se ci sono nan nel dataset. Se da un array vuoto vuol dire che non ci sono"""
    k=np.isnan(Y.any())
    print(k[k.values==True])    
    
    
    
    
    

    
                                                           
                       