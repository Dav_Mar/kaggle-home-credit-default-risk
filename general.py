#18/08/2018




def dict(dataset,col='name2',p=2, number='no'):      #create a dictionary
    
 """Questa funzione crea un dizionario con chiavi AA. AB, AC, ..., ZZ. Si possono fare anche tre lettere e mettere numeri
    
  dataset='name pandas dataset' , col='name of column', p=2/3 'number of permutations       
  number='no/yes if include just alphabetic number or number too'"""

 if (number=='yes'):
   a=['A', 'B', 'C', 'D', 'E','F','G','H', 'I','J','K','L','M', 'N', 'O', 'P' ,'Q','R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z','0','1','2','3','4','5','6','7','8','9']
 else:
   a=['A', 'B', 'C', 'D', 'E','F','G','H', 'I','J','K','L','M', 'N', 'O', 'P' ,'Q','R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z']
  
 k=[0]*len(a)**2
 t=0
 for j in a:
  for i in a:
       k[t]=j+i
       k[t]=k[t].lower()
       t=t+1
        
 if(p==3):     
   k2=[0]*len(k)**2
   t=0
   for j in k:
     for i in a:
        k2[t]=j+i
        k2[t].lower()
        t=t+1
     k=k2
 else:
      k=k
 
 c={}
 for i in k:
     t=dataset.loc[dataset[col] == i]
     c[i]=t
 
 return(c) 





def name_to_gender(name,d):    
    
  """ CONVERTE NOME A SESSO"""
    
  if(len(name)<3):            
    name='aa'
  k=d[name[0:2]]
     
  t=k[['gender','name','probability']].loc[k['name'] == name] 

  if t.shape[0]==0: 
      t='NaN'
  else:    
      t=t.sort_values('probability')['gender'].iloc[0]
  return(t)    
