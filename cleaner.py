#18/08/2018

import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import numpy as np
from sklearn.model_selection import train_test_split
from scipy.stats import ranksums
from lightgbm import LGBMClassifier
import pandas as pd
from sklearn.metrics import roc_auc_score, precision_score, recall_score
import statistica as st
from imblearn.over_sampling import SMOTE



def leggi_csv(file, sep, titoli):
    """carica un file csv in un data frame"""
    if(titoli=="No"): 
        df = pd.read_csv(file, header=None, sep=sep)
    else:
        df = pd.read_csv(file, sep=sep)

    return df



def df_to_matrix(df):
    """converte dataframe a matrice"""
    return df.values

def csv_to_matrix(file, sep):
    """Prende un csv e lo converte in matrix"""
    df = pd.read_csv(file, header=None, sep=sep)
    return df.values

def standard_matrix(x):
    """Trasforma matrice con valori standardizzati"""

    df_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
    df = df_scaler.fit_transform(x)
    return df

def matrix_to_df(matrix):
    """converte dataframe a matrice"""
    return pd.DataFrame(matrix)

def reshape(x,colonne):
    """converte matrice (n,m) in una matrice (n/k, k)"""
    return x.reshape(int((x.shape[0]*x.shape[1])/colonne),colonne)

def append_matrix(x,y):
    """appende per colonna due matrici"""
    return np.append(x,y, axis=0)

def divisori(num):
    """calcola divisori di un numero"""
    for i in range(1,num+1):
        if (num%i==0):
            print(i)
    
def estrai_target(y):
    """dato un dataset (n,m) crea un train (n,m-1) e un test (n,1) ultima colonna"""
    x=y[:,0:(y.shape[1]-1)]
    y=y[:,(y.shape[1]-1)]
    return x,y

def differenza(x,y):
    """ data matrice x (n,m) e vettore y (n,1) calcola nuova matrice (n,m) dove x[i,j]=(x[i,j]-y[i]) / y[i]"""
    for i in range(0,x.shape[0]):
        for j in range(0,x.shape[1]):
            x[i,j]=(x[i,j]-y[i])/y[i]
    return x

def differenza2(x):
    """ data un vettore, crea un nuovo vettore dove y(t)=x(t)-x(t+1)"""
    y=x
    k2=x.columns
    for i in range(0,len(k2)-1):
       y[k2[i]]=x[k2[i]]-x[k2[i+1]]
    return y

def duplicati_e_nan(x):
    """ dato un dataframe, toglie le righe duplicate e le righe con nan"""
    x=x.dropna()
    x=x.drop_duplicates()
    return x

def dividi_dataset(x,var, perc=0.7):
    """ data un dataset X dove si ha la variabile target 'var', si divide in training set e test set
    dove il training sarà formato per 'perc' di X, ad esempio perc=0.7 sarà formato 70% training e 30% test"""
    X=x.loc[:, x.columns != var]
    cl1=x[var]
    x_train, x_test, y_train, y_test = train_test_split(X, cl1, test_size = 1-perc)
    return x_train, x_test, y_train, y_test

def dividi_dataset2(x, perc=0.7):
    """data una matrice X dove l'ultima variabile è la variabile target, si si divide in training set e test set
    dove il training sarà formato per 'perc' di X, ad esempio perc=0.7 sarà formato 70% training e 30% test"""
    X=x[:,0:x.shape[1]-1]
    cl1=x[:,x.shape[1]-1]
    r=round(x.shape[0]*(1-perc))
    x_train=X[0:r]
    x_test=cl1[0:r]
    y_train=X[r:]
    y_test=cl1[r:]

    return x_train, x_test, y_train, y_test


def dividi_dataset3(x,var='TARGET', perc=0.7):
    """ data un dataset X senza la variabile target e dato un altro dataset 'var' che contiene la variabile TARGET,
    si divide in training set e test set dove il training sarà formato per 'perc' di X, ad esempio perc=0.7 
    sarà formato 70% training e 30% test"""
    X=x
    cl1=var
    x_train, x_test, y_train, y_test = train_test_split(X, cl1, test_size = 1-perc)
    return x_train, x_test, y_train, y_test


def salvacsv(df,nome='df.csv',sep=',',encoding='utf-8', index=False):
    """salva il dataframe 'df' come csv, con separator , e mettendo come prima riga il nome delle variabili"""
    print("salvato il dataset con nome "+nome)
    df.to_csv(nome, sep=sep,encoding=encoding, index=index)
    
    
def dropcol(df,colonne):
    """dato il dataset df si droppa le colonne=['cola','colb',...]"""
    df=df.drop(colonne, axis = 1)
    return df

def rimpiazza_na(df,parola):
    """rimpiazza i 'nan' con 'parola' al dataset df"""
    return df.fillna(parola)

def duplicati(df):
    """droppa tutte le righe duplicate e se tiene solo una"""
    return df.drop_duplicates()

def rinomina(df,a):
    """rinomina dataframe come a={'oldName1': 'newName1', 'oldName2': 'newName2'....}"""
    return df.rename(columns=a, inplace=True)

def merge_dataset(df1,df2):
    """esegue merge per colonna dei due dataset"""
    return pd.concat([df1, df2], axis = 0, ignore_index = True)


def var_ripetive(x, percentuale=0.8,TARGET='TARGET'):
    """toglie tutte le variabili, ad eccezione della variabile TARGET di nome 'TARGET', che hanno una modalità
    che assume piu del 'percentuale' %, ad esempio se 0.8, se assume piu del 80%"""
    a=x.dtypes.index
    a=a[a!=TARGET]
    y=x[TARGET]
    for i in x.dtypes.index:
        if (x[i].value_counts().max()/x.count()[i]> percentuale):
            print("rimuovi: "+str(i))
            x=dropcol(x,i)
        else:
            print("no rimuovi")
            
    x[TARGET]=y   

    return x


def uniche(data):
    """droppa le variabili che assumono solo una modalità"""
    nun = data.nunique()     #se assumono solo un valore vengono eliminate
    empty = list(nun[nun <= 1].index)
    
    data.drop(empty, axis = 1, inplace = True)
    print('After removing empty features there are {0:d} features'.format(data.shape[1]))
    return data

def dropcolonne_NaN(df,index='ID', perc=0.25):
    """droppa tutte le variabili che hanno piu del 25% Nan"""
    for i in st.var(df):
        a=df[i].isnull().sum()/df[index].count()
        if(a>0.25):
            df=dropcol(df,i)
        print(i+"   "+str(a))
    return df

def var_binaria(x,var,a,a2,b,b2):
    """data una variabile var di un dataset, si sostistuisce i valori a in a2 e i valori b in b2,
    ad esempio X[var]=2,9,5 con var_binaria(X,var, 5,10,2,4) si ottiene che
    X[var]=4,9,10"""
    return  x[var].replace(a,a2).replace(b,b2)


def array_to_float(x,var):
    """converte una variabile var del dataset x in float"""
    return x[var].astype(float)
                         
def pandas_to_float(df):
    """converte dataframe in float"""
    df=df.astype(np.float32)
    return df                         


def drop_row(df,var,valore):
    """droppa tutte le righe dove nel dataset x alla variabile var assume modalità 'valore'"""
    return df.drop(df[df[var] == valore].index, inplace = True)


        
def corr_feature_with_target(feature, target,target0=0,target1=1):
    """serve per la funzione dopo"""
    c0 = feature[target == target0].dropna()
    c1 = feature[target == target1].dropna()
        
    if set(feature.unique()) == set([0, 1]):
        diff = abs(c0.mean(axis = 0) - c1.mean(axis = 0))
    else:
        diff = abs(c0.median(axis = 0) - c1.median(axis = 0))
        
    p = ranksums(c0, c1)[1] if ((len(c0) >= 20) & (len(c1) >= 20)) else 2
        
    return [diff, p]


def stessa_distribuzione(data,TARGET='TARGET',INDEX='SK_ID_CURR',target0=0,target1=1):
    """Dato il target='TARGET' e l'indice indincandone l'id nel dataset 'INDEX',
    rimuove le variabili dove la distribuzione target per modalità 'target0' e uguale a
    quella della modalità 'target1'. In poche parole per quelle variabili che hanno
    stessa distribuzione per le due modalità del target, vengono eliminate"""


    corr = pd.DataFrame(index = ['diff', 'p'])
    ind = data[data[TARGET].notnull()].index   #prende gli indici dove il target non è null
    
    for c in data.columns.drop(TARGET):
        corr[c] = corr_feature_with_target(data.loc[ind, c], data.loc[ind, TARGET],target0,target1)

    corr = corr.T
    corr['diff_norm'] = abs(corr['diff'] / data.mean(axis = 0))
    
    to_del_1 = corr[((corr['diff'] == 0) & (corr['p'] > .05))].index
    to_del_2 = corr[((corr['diff_norm'] < .5) & (corr['p'] > .05))].drop(to_del_1).index
    to_del = list(to_del_1) + list(to_del_2)
    if INDEX in to_del:
        to_del.remove(INDEX)
        
    data.drop(to_del, axis = 1, inplace = True)
    print('After removing features with the same distribution on 0 and 1 classes there are {0:d} features'.format(data.shape[1]))
    
    #same distribution
    corr_test = pd.DataFrame(index = ['diff', 'p'])
    target = data[TARGET].notnull().astype(int)
    
    for c in data.columns.drop(TARGET):
        corr_test[c] = corr_feature_with_target(data[c], target,target0,target1)

    corr_test = corr_test.T
    corr_test['diff_norm'] = abs(corr_test['diff'] / data.mean(axis = 0))
    
    bad_features = corr_test[((corr_test['p'] < .05) & (corr_test['diff_norm'] > 1))].index
    bad_features = corr.loc[bad_features][corr['diff_norm'] == 0].index
    
    data.drop(bad_features, axis = 1, inplace = True)
    print('After removing features with not the same distribution on train and test datasets there are {0:d} features'.format(data.shape[1]))
    return data



def var_nointeressanti(data,TARGET='TARGET'):
    """rimuove le variabili ritnute non interessanti"""
    clf = LGBMClassifier(random_state = 0)
    train_index = data[data['TARGET'].notnull()].index
    train_columns = data.drop('TARGET', axis = 1).columns

    score = 1
    new_columns = []
    while score > .7:
        train_columns = train_columns.drop(new_columns)
        clf.fit(data.loc[train_index, train_columns], data.loc[train_index, 'TARGET'])
        f_imp = pd.Series(clf.feature_importances_, index = train_columns)
        score = roc_auc_score(data.loc[train_index, 'TARGET'], 
                              clf.predict_proba(data.loc[train_index, train_columns])[:, 1])
        new_columns = f_imp[f_imp > 0].index

    data.drop(train_columns, axis = 1, inplace = True)
    print('After removing features not interesting for classifier there are {0:d} features'.format(data.shape[1]))
    return data





def oversampling(x,t=2):
    """dato un dataset x viene creato un nuovo dataset y con merge del dataset x t volte"""
    x1=x
    for i in range(1,t):
        x=mergrimpiazza_median(T)e_dataset(x1,x)
    return x


def undersampling(a,frac=0.5,replace=False):
    """dato un dataset a viene creato un nuovo dataset dove è formato da estrazione casuale, senza
    replace. Ad esempio 0.5 significa che si prenderà il 505"""

    a=a.sample(frac=frac, replace=replace)
    return a


def SMOTE2(X,Y,random_state=12, ratio = 1,perc=0.7):
    """prende dataset train X e quello di output Y e se output ha una classe minore ad esempio a 10% 0 e 90% 1, con ratio 1
    estrapola altri record di X per garantire che si ha 50% 0 e 50% 1. Poi esegue uno split del dataset formato in trainig
    test set con percentuale perc, ad esempio perc=0.7 fa si che si ha 70& training e 30% test."""
    sm = SMOTE(random_state=random_state, ratio = ratio)
    x_res, y_res = sm.fit_sample(X,Y)
    x_train_res, x_val_res, y_train_res, y_val_res=dividi_dataset3(x_res,y_res, perc)
    return x_train_res, x_val_res, y_train_res, y_val_res






def rimpiazza_median(T):
    """rimpiazza a un dataset T in tutte le variabili, i nan con la mediana."""
    for i in st.var(T):
        T[i]=rimpiazza_na(T[i],T[i].median())
    return T





    

